fun main() {
    val x = Fraction(3.0, 6.0)
    println(x)
    val y = Fraction( 6.0, 8.0)
    println(y)
    println(x==y)
    println(x.add(y))
    println(x.multiply(y))
    println(x.minus(y))

}

class Fraction(val nominator: Double, val denominator: Double) {
    override fun toString(): String {
        return "$nominator $denominator"
    }

    override fun equals(other: Any?): Boolean {
        if (other is Fraction) {
            return (nominator * other.denominator == other.nominator * denominator)
        }
        return false
    }
    fun add(other: Fraction): Fraction{
        val newDenominator = denominator * other.denominator
        val newNominator1 = newDenominator / denominator * nominator
        val newNominator2 = newDenominator / other.denominator * other.nominator
        return Fraction (newNominator1 + newNominator2, newDenominator)
    }
    fun multiply(other: Fraction): Fraction {
        val newNominator = nominator * other.nominator
        val newDenominator = denominator * other.denominator
        return Fraction(newNominator, newDenominator)
    }
    fun minus(other: Fraction): Fraction {
        return add(Fraction(-1 * other.nominator, other.denominator))
    }
    fun divide(other: Fraction): Fraction {
        return multiply(Fraction(other.denominator,other.nominator))
    }


}